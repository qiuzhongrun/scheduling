package qiuzhongrun.scheduling.annotation;

import org.springframework.context.annotation.Import;
import qiuzhongrun.scheduling.util.ScheduleMonitor;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(ScheduleMonitor.class)
@Documented
public @interface SchedulingEnabled {
}
