package qiuzhongrun.scheduling.util;

import java.util.Date;

public class Cron {
    private String cron;
    private String key;
    private Date nextExecDate;

    public Cron() {}

    public Cron(String cron) {
        this.cron = cron;
    }

    public Cron(String cron, String key) {
        this.cron = cron;
        this.key = key;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getNextExecDate() {
        return nextExecDate;
    }

    public void setNextExecDate(Date nextExecDate) {
        this.nextExecDate = nextExecDate;
    }

    @Override
    public String toString() {
        return "[key="+key +
                "cron=" + cron +
                "nextExecDate"+ nextExecDate +
                "]";
    }
}
