package qiuzhongrun.scheduling.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Configuration
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class ScheduleMonitor {
    String regEx = "(((^([0-9]|[0-5][0-9])(\\,|\\-|\\/){1}([0-9]|[0-5][0-9]))|^([0-9]|[0-5][0-9])|^(\\* ))((([0-9]|[0-5][0-9])(\\,|\\-|\\/){1}([0-9]|[0-5][0-9]) )|([0-9]|[0-5][0-9]) |(\\* ))((([0-9]|[01][0-9]|2[0-3])(\\,|\\-|\\/){1}([0-9]|[01][0-9]|2[0-3]) )|([0-9]|[01][0-9]|2[0-3]) |(\\* ))((([0-9]|[0-2][0-9]|3[01])(\\,|\\-|\\/){1}([0-9]|[0-2][0-9]|3[01]) )|(([0-9]|[0-2][0-9]|3[01]) )|(\\? )|(\\* )|(([1-9]|[0-2][0-9]|3[01])L )|([1-7]W )|(LW )|([1-7]\\#[1-4] ))((([1-9]|0[1-9]|1[0-2])(\\,|\\-|\\/){1}([1-9]|0[1-9]|1[0-2]) )|([1-9]|0[1-9]|1[0-2]) |(\\* ))(([1-7](\\,|\\-|\\/){1}[1-7])|([1-7])|(\\?)|(\\*)|(([1-7]L)|([1-7]\\#[1-4]))))|(((^([0-9]|[0-5][0-9])(\\,|\\-|\\/){1}([0-9]|[0-5][0-9]) )|^([0-9]|[0-5][0-9]) |^(\\* ))((([0-9]|[0-5][0-9])(\\,|\\-|\\/){1}([0-9]|[0-5][0-9]) )|([0-9]|[0-5][0-9]) |(\\* ))((([0-9]|[01][0-9]|2[0-3])(\\,|\\-|\\/){1}([0-9]|[01][0-9]|2[0-3]) )|([0-9]|[01][0-9]|2[0-3]) |(\\* ))((([0-9]|[0-2][0-9]|3[01])(\\,|\\-|\\/){1}([0-9]|[0-2][0-9]|3[01]) )|(([0-9]|[0-2][0-9]|3[01]) )|(\\? )|(\\* )|(([1-9]|[0-2][0-9]|3[01])L )|([1-7]W )|(LW )|([1-7]\\#[1-4] ))((([1-9]|0[1-9]|1[0-2])(\\,|\\-|\\/){1}([1-9]|0[1-9]|1[0-2]) )|([1-9]|0[1-9]|1[0-2]) |(\\* ))(([1-7](\\,|\\-|\\/){1}[1-7] )|([1-7] )|(\\? )|(\\* )|(([1-7]L )|([1-7]\\#[1-4]) ))((19[789][0-9]|20[0-9][0-9])\\-(19[789][0-9]|20[0-9][0-9])))";

    private final Log logger = LogFactory.getLog(ScheduleMonitor.class);
    private Map<String, Task> TASK_MAP = new ConcurrentHashMap<>();

    protected void put(String key, Method method, Cron cron) {
        notNullAssert(key, method, cron);

        if (TASK_MAP.containsKey(key)) {
            Task task = TASK_MAP.get(key);
            Method oldMethod = task.getMethod();
            String methodName0 = oldMethod.getName();
            String methodName1 = oldMethod.getName();
            String msg = "Duplicate key for scheduling task. " +
                            "key["+key+"],"+
                            "methodName0["+methodName0+"]"+
                            "methodName1["+methodName1+"]";

            logger.error(msg);
        } else {
            Task task = new Task(method, cron);
            TASK_MAP.put(key, task);
        }
    }

    private static void notNullAssert(String key, Method method, Cron cron) {
        Assert.notNull(key != null, "Key can be null.");
        Assert.notNull(method != null, "Method can be null.");
        Assert.notNull(cron != null, "Cron can be null.");
    }

    public boolean changeCron(String key, String newCron) {
        if (StringUtils.isEmpty(key) || StringUtils.isEmpty(newCron) ) {
            logger.error("Key["+key+"] or cron["+newCron+"] is empty.");
            return false;
        }

        if (!newCron.matches(regEx)) {
            logger.error("Key["+key+"]'s cron["+newCron+"] is not satisfied cron expression.");
        }

        Task task = TASK_MAP.get(key);
        if (task==null) {
            logger.warn("Task for key["+key+"] not exist.");
            return false;
        } else {
            task.getCron().setCron(newCron);
            return true;
        }
    }

    @Bean(name = "com.qiuzhongrun.scheduling.bean.internalAnnotationProcessor")
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public AnnotationBeanPostProcessor annotationProcessor() {
        return new AnnotationBeanPostProcessor();
    }

    @Bean
    public TaskScheduler poolScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setThreadNamePrefix("poolScheduler");
        scheduler.setPoolSize(100);
        return scheduler;
    }
}
