package com.qiuzhongrun.schedulingtest.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qiuzhongrun.scheduling.annotation.Scheduling;
import qiuzhongrun.scheduling.util.ScheduleMonitor;


@Service
public class TestScheduling {
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private ScheduleMonitor scheduleMonitor;

    private final String cronKey = "schedulingTest";

    @Scheduling(cron = "*/5 * * * * ?", key = cronKey)
    public void schedulingTest() {
        logger.info("Hi, beauty!");
    }

}
