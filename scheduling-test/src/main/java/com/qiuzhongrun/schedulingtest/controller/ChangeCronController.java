package com.qiuzhongrun.schedulingtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import qiuzhongrun.scheduling.util.Cron;
import qiuzhongrun.scheduling.util.ScheduleMonitor;

@RestController
public class ChangeCronController {
    @Autowired
    private ScheduleMonitor scheduleMonitor;

    @RequestMapping(value = "/change-cron", method = RequestMethod.POST)
    public String queryMiss(@RequestBody Cron cron) {
        boolean isSuccess = scheduleMonitor.changeCron(cron.getKey(), cron.getCron());
        String result = isSuccess==true?"Success!":"Failed!";
        return "Change "+cron.getKey() +"'s cron to '" +cron.getCron() +"' is "+ result;
    }

}
