package com.qiuzhongrun.schedulingtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import qiuzhongrun.scheduling.annotation.SchedulingEnabled;

@SchedulingEnabled
@SpringBootApplication
public class SchedulingTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SchedulingTestApplication.class, args);
    }

}
