一个基于spring-context的简单的可动态修改的定时任务调度框架。

A simple Schedule framework base on spring-context, 

it can Change the Period of the Scheduling Task by  interface invoke without restarting.

# Usage
To use scheduling, you do the following
1) Enable and configure SchedulingEnabled
2) Annotate your scheduling tasks
3) Change cron by key


### Enable and configure SchedulingEnabled
First of all we have to import the project

```xml
<dependency>
    <groupId>com.qiuzhongrun</groupId>
    <artifactId>scheduling</artifactId>
    <version>1.0</version>
</dependency>
```

Now we need to integrate the library into Spring. In order to enable scheduling  use `@SchedulingEnabled` annotation

```java
@SchedulingEnabled
@SpringBootApplication
public class SchedulingTestApplication {
    ...
}
```

### Annotate your scheduling tasks
 
 ```java
import qiuzhongrun.scheduling.annotation.Scheduling;

...

@Scheduling(cron = "*/5 * * * * ?", key = "schedulingTest")
public void schedulingTest() {
    // do something
}
```


### Change your scheduling task's cron by ScheduleMonitor
 
 ```java
import org.springframework.beans.factory.annotation.Autowired;
import qiuzhongrun.scheduling.util.Cron;
import qiuzhongrun.scheduling.util.ScheduleMonitor;

...

@Autowired
private ScheduleMonitor scheduleMonitor;

public void change(String key, String cron) {
    scheduleMonitor.changeCron(key, cron);
}
```


There is an usage sample [scheduling-test](https://gitlab.com/qiuzhongrun/scheduling/tree/master/scheduling-test).

Enjoy it!